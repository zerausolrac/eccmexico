// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "eccpue",
    dependencies: [
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.6"),
        .package(url: "https://github.com/vapor/fluent-mysql.git", from: "3.0.0-rc.4.0.1"),
        .package(url: "https://github.com/vapor/leaf.git", from: "3.0.0-rc.2.2"),
        .package(url: "https://github.com/vapor/auth.git", from: "2.0.0-rc.5"),
        .package(url: "https://github.com/twof/VaporMailgunService.git", from: "1.0.1"),
        .package(url: "https://github.com/LiveUI/S3.git", .branch("master")),
    ],
    targets: [
        .target(name: "App", dependencies: ["FluentMySQL", "Vapor","Authentication", "Leaf","Mailgun","S3"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
    ]
)

