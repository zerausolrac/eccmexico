import Routing
import Vapor


public func routes(_ router: Router) throws {
    
    //User web-based
    let userController = UserController()
    try router.register(collection: userController)

    
    //API token-based
    let apiContrller = APIController()
    try router.register(collection: apiContrller)
    
    // Recursos routes
    let recursoController = RecursoController()
    try router.register(collection: recursoController)
    
    //Categoria routes
    let categoriaController = CategoriaController()
    try router.register(collection: categoriaController)
    
    //Articulo routes
    let articuloController = ArticuloController()
    try router.register(collection: articuloController)
    
    //WebController
    let webController = WebController()
    try router.register(collection: webController)
    
    //RegistroController
    let registroController = RegistroController()
    try router.register(collection: registroController)
    
}
