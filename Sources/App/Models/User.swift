import Vapor
import FluentMySQL
import Authentication


enum Roles:Int  {
    case none = 0
    case admin = 1
    case editor = 2
    
}

extension Roles: MySQLDataTypeStaticRepresentable, ReflectionDecodable, MySQLDataConvertible {
    
    static var mySQLColumnDefinition: MySQLColumnDefinition.DataType {
        return MySQLColumnDefinition.DataType.tinyint()
    }
    
    static func reflectDecoded() throws -> (Roles, Roles) {
        return (.none, .admin)
    }
    
    private func convertToMySQLData() throws -> MySQLData {
        return MySQLData(integer: self.rawValue)
    }
    
    static func convertFromMySQLData(_ mysqlData: MySQLData) throws -> Roles {
        return Roles(rawValue: try convertFromMySQLData(mysqlData).rawValue)!
    }
    
    
    
}


final class User:Codable, CustomStringConvertible{
    var description: String {return "username:\(username) role:\(role!) name:\(name!)"}
    

    var id:Int?
    var username:String
    var password:String
    var name:String?
    var creado:Date?
    var actualizado:Date?
    var tokenActivacion:String?
    var usuarioActivo:Bool?
    var role:Int?
    
    
 
    init(username:String , password:String){
        self.username = username
        self.password = password
    }
}


//Relationship User-Recurso
extension User {
    var recursosCreados: Children<User,Recurso>{
        return children(\.creadorID)
    }
    
    var articulos:Children<User,Articulo>{
        return children(\.autor)
    }
}



extension User:Model{
    
    typealias Database = MySQLDatabase
    typealias ID = Int
    static var idKey: IDKey {return \.id}
}

extension User {
    static var createdAtKey: TimestampKey {
        return \.creado
    }
    static var updatedAtKey: TimestampKey {
        return \.actualizado
    }
}


extension User:CamposAgregables {
    static var role: RoleKey {
         return \.role
    }
    
    static var nameKey: NameKey {
        return \.name
    }
    
    static var usuarioActivoKey: UsuarioActivoKey {
        return \.usuarioActivo
    }

    static var tokenActivacionKey: TokenActivacionKey {
        return \.tokenActivacion
    }
    
    
    
}




extension User:Migration {
    static func prepare(on connection: Database.Connection) -> Future<Void>{
        
        return Database.create(User.self, on: connection, closure: { (builder:SchemaCreator<User>) in
            //try addProperties(to: builder)
             builder.field(for: \User.id)
             builder.field(for: \User.username)
             builder.field(for: \User.password)
             builder.field(for: \User.name)
             builder.field(for: \User.creado)
             builder.field(for: \User.actualizado)
            
        
            let tokenActivacion = MySQLColumnDefinition.columnDefinition(MySQLColumnIdentifier.column(MySQLTableIdentifier?.none, MySQLIdentifier.identifier("tokenActivacion")), MySQLDataType.varchar(255), [])


            let usuarioActivo = MySQLColumnDefinition.columnDefinition(MySQLColumnIdentifier.column(MySQLTableIdentifier?.none, MySQLIdentifier.identifier("usuarioActivo")), MySQLDataType.bool, [])


            let role = MySQLColumnDefinition.columnDefinition(MySQLColumnIdentifier.column(MySQLTableIdentifier?.none, MySQLIdentifier.identifier("role")), MySQLDataType.tinyint, [])

             builder.field(tokenActivacion)
             builder.field(usuarioActivo)
             builder.field(role)
            
            
            
            
            
//             builder.addField(type: MySQLColumnDefinition.varChar(length: 255), name: "tokenActivacion")
//             builder.addField(type: MySQLColumnDefinition.bool(), name: "usuarioActivo")
//             builder.addField(type: MySQLColumnDefinition.tinyInt(), name: "role")
            
            builder.unique(on: \.username)
            
        })
        
    }
    
}
extension User:Content{}
extension User:Parameter{}


//Password Authentication
extension User:PasswordAuthenticatable{
    static var usernameKey: UsernameKey {
        return \.username
    }
    
    static var passwordKey: PasswordKey {
        return \.password
    }
}

extension User:SessionAuthenticatable{}


//Token Authentocation

extension User:TokenAuthenticatable {
  typealias TokenType = Token
}



//Validation
extension User:Validatable {
    static func validations() throws -> Validations<User> {
        var validaciones = Validations(self)
         try validaciones.add(\.password, .alphanumeric)
        return validaciones
    }
}


//Relationship

extension User {
    var user:Children<User, Token>{
        return children(\.userID)
    }
    
}


//Custom Database entries

public protocol CamposAgregables:CualquierCampoAgrebable{
    typealias UsuarioActivoKey = WritableKeyPath<Self, Bool?>
    typealias TokenActivacionKey = WritableKeyPath<Self,String?>
    typealias NameKey = WritableKeyPath<Self,String?>
    typealias RoleKey = WritableKeyPath<Self,Int?>
    
    static var usuarioActivoKey: UsuarioActivoKey { get }
    static var tokenActivacionKey: TokenActivacionKey { get }
    static var nameKey: NameKey { get }
    static var role: RoleKey { get }
}


extension CamposAgregables {
    public var fluentUsurioActivo:Bool?{
        get{ return self[keyPath:Self.usuarioActivoKey]}
        set{ self[keyPath:Self.usuarioActivoKey] = newValue}
    }

    public var fluentTokenActivacion:String? {
        get{return self[keyPath:Self.tokenActivacionKey]}
        set{ self[keyPath:Self.tokenActivacionKey] = newValue}
    }
 
    public var fluentName:String? {
    get{ return self[keyPath:Self.nameKey]}
        set{ self[keyPath:Self.nameKey] = newValue }
    }
    
    
    public var fluentRole:Int?{
        get{ return self[keyPath:Self.role]}
        set{ self[keyPath:Self.role] = newValue}
    }
    
}

public protocol CualquierCampoAgrebable:AnyModel{
    var fluentUsurioActivo:Bool? {get set}
    var fluentTokenActivacion:String? {get set}
    var fluentName:String? {get set}
    var role:Int? { get set }
}




