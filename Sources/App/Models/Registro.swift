import Vapor
import FluentMySQL


final class Registro:Codable {
 
    var id:UUID?
    var nombre:String
    var email:String
    var clase:String
    var area:String
    var embajador:Int
    var autor:Int
    var consejero:Int
    var abogado:Int
    var razon:String
    var equipo:Int
    var apps:[String]?
    var razon2:String
    var alergia:Int
    var playera: String
    var agree:Int
    var creado:Date?
    var actualizado:Date?
    
    
    init(
     nombre:String,
     email:String,
     clase:String,
     area:String,
     embajador:Int,
     autor:Int,
     consejero:Int,
     abogado:Int,
     razon:String,
     equipo:Int,
     apps:[String]?,
     razon2:String,
     alergia:Int,
     playera: String,
     agree:Int) {
        
        //self.id = id
        self.nombre = nombre
        self.email = email
        self.clase = clase
        self.area = area
        self.embajador = embajador
        self.autor = autor
        self.consejero = consejero
        self.abogado = abogado
        self.razon = razon
        self.equipo = equipo
        self.apps = apps
        self.razon2 = razon2
        self.alergia = alergia
        self.playera = playera
        self.agree = agree
        
        
    }
}


 extension Registro:Model{
    typealias Database = MySQLDatabase
    typealias ID = UUID
    static var idKey:IDKey {return \.id}
 }


extension Registro{
    static var createdAtKey: TimestampKey {
        return \.creado
    }
    
    static var updatedAtKey: TimestampKey {
        return \.actualizado
    }
}


 extension Registro: Migration{
    static func prepare(on conn: Database.Connection) -> Future<Void> {
        return Database.create(Registro.self, on: conn) { builder in

            builder.field(for: \.id)
            builder.field(for:\.nombre, type:.varchar(255))
            builder.field(for:\.email, type:.varchar(255))
            builder.field(for:\.clase, type:.varchar(255))
            builder.field(for:\.area, type:.varchar(255))
            builder.field(for:\.embajador, type:MySQLDataType.tinyint)
            builder.field(for:\.autor, type:MySQLDataType.tinyint)
            builder.field(for:\.consejero, type:MySQLDataType.tinyint)
            builder.field(for:\.abogado, type:MySQLDataType.tinyint)
            builder.field(for:\.razon, type:.varchar(255))
            builder.field(for:\.equipo, type:MySQLDataType.tinyint)
            builder.field(for:\.apps, type:.varchar(255))
            builder.field(for:\.razon2, type:.varchar(255))
            builder.field(for:\.alergia, type:MySQLDataType.tinyint)
            builder.field(for:\.playera, type:.varchar(255))
            builder.field(for:\.agree, type:MySQLDataType.tinyint)
            builder.field(for: \.creado, type:.datetime(6))
            builder.field(for: \.actualizado, type:.datetime(6))
            
        }
    }
 
}



extension Registro:Content{}
extension Registro:Parameter{}



