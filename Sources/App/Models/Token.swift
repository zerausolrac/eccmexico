import Vapor
import FluentMySQL
import Crypto
import Authentication

final class Token: Codable {
    
    var userID: User.ID
    
    var id:Int?
    //var username:String
    var tokenCode:String
    var creado:Date?
    var actualizado:Date?
    
    init(tokenCode:String, userId:User.ID){
       // self.username = username
        self.tokenCode = tokenCode
        self.userID = userId
    }
}


extension Token:Model{
    typealias Database = MySQLDatabase
    typealias ID = Int
    static var idKey: IDKey{ return \.id}
}


extension Token{
    static var createdAtKey: TimestampKey {
        return \.creado
    }
    
    static var updatedAtKey: TimestampKey {
        return \.actualizado
    }
    
}

extension Token:Migration{
    
    static func prepare(_ connection:Database.Connection) -> Future<Void>{
         return Database.create(Token.self, on: connection, closure: { builder in
            try addProperties(to: builder)
             builder.unique(on: \.userID)
        })
    }
}



extension Token:Content{}
extension Token:Parameter{}


//Token conformance

extension Token:Authentication.Token {
    typealias UserIDType = User.ID
    typealias UserType = User
    static var userIDKey: UserIDKey { return \.userID }
    static var tokenKey: TokenKey { return \.tokenCode }
}



//Token generation
extension Token{
    static func generarToken(_ req:Request,user:User) throws -> Token{
        let random = try CryptoRandom().generateData(count: 16)
        return  try Token(tokenCode: random.base64EncodedString(), userId: user.requireID())
    }
}




