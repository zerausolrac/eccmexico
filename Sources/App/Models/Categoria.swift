
import Vapor
import FluentMySQL

final class Categoria:Codable {

    var id:Int?
    var nombreCategoria:String
    
    init(nombreCategoria:String) {
        self.nombreCategoria = nombreCategoria
    }
}

extension Categoria: Model{
    typealias ID = Int
    typealias Database = MySQLDatabase
    static var idKey:IDKey{return \.id}
}


//Relationship
extension Categoria{

    var recursos: Siblings<Categoria, Recurso, RecursoCategoriaPivot>{
     return siblings()
    }

    var articulos: Siblings<Categoria,Articulo, ArticuloCategoriaPivot>{
        return siblings()
    }

}

extension Categoria:Content{}
extension Categoria:Migration{}
extension Categoria:Parameter{}
