import Vapor
import FluentMySQL


final class Articulo:Codable{
    
    var id:UUID?
    var titulo:String
    var cuerpo:String
    var visible:Bool
    var creado:Date?
    var actualizado:Date?
    
    //Relationship -Parent_Children
    var autor: User.ID
    
    
    init(titulo:String, cuerpo:String, visible:Bool, autor:User.ID) {
        self.titulo = titulo
        self.cuerpo = cuerpo
        self.visible = visible
        //self.categoria = categoria
        self.autor = autor
    }
}


extension Articulo{
    var creador: Parent<Articulo,User> {
        return parent(\.autor)
    }
    
    var articuloCategorias: Siblings<Articulo, Categoria, ArticuloCategoriaPivot>{
        return siblings()
    }
    
    
}

extension Articulo{
    
    static var createdAtKey: TimestampKey {
        return \.creado
    }
    
    static var updatedAtKey: TimestampKey {
        return \.actualizado
    }
    
}


extension Articulo:Model{
    typealias Database = MySQLDatabase
    typealias ID = UUID
    static var idKey: IDKey {
        return \.id
    }
}


extension Articulo:Migration{
    static func prepare(on connection: MySQLConnection) -> EventLoopFuture<Void> {
        return Database.create(Articulo.self, on: connection, closure: { (builder) in
            builder.field(for: \.id)
            builder.field(for: \.titulo, type: .varchar(255))
            builder.field(for: \.cuerpo, type: .longtext)
            builder.field(for: \.visible, type: .bool)
            builder.field(for: \.creado, type:.datetime(6))
            builder.field(for: \.actualizado, type:.datetime(6))
            builder.field(for: \.autor)
        })
        
        
    }
    
    static func revert(on connection: MySQLConnection) -> EventLoopFuture<Void> {
        return Database.delete(Articulo.self, on: connection)
    }
    
}

extension Articulo:Content{}
extension Articulo: Parameter{}







