import Vapor
import FluentMySQL


enum TipoRecurso:String,Codable {
    case video,audio,pdf,url
}

extension TipoRecurso: MySQLDataTypeStaticRepresentable ,ReflectionDecodable, MySQLDataConvertible{
    
    private func convertToMySQLData() throws -> MySQLData {
        return MySQLData(string: self.rawValue)
    }
    
    static func convertFromMySQLData(_ mysqlData: MySQLData) throws -> TipoRecurso {
        return TipoRecurso(rawValue: mysqlData.string(encoding: .utf8)!)!
    }
    
    static var mySQLColumnDefinition: MySQLColumnDefinition.DataType {
        return MySQLColumnDefinition.DataType.text()
    }
    
    static func reflectDecoded() throws -> (TipoRecurso, TipoRecurso) {
        return (.video,.audio)
    }
}



final class Recurso:Codable{
    var id:UUID?
    var titulo:String
    var urlStorage:String
    var descripcion:String
    var disponible:Bool
    var tipo:TipoRecurso
    var creado:Date?
    var actualizado:Date?
    
    //Relationship
    var creadorID:User.ID
    
    
    init(titulo:String, urlStorage:String, descripcion:String, disponible:Bool, tipo:TipoRecurso, creadorID:User.ID) {
        self.titulo = titulo
        self.urlStorage = urlStorage
        self.descripcion = descripcion
        self.disponible = disponible
        self.tipo = tipo
        
        //relationship
        self.creadorID = creadorID
    }
 }


//Relationship
extension Recurso{
    var creador:Parent<Recurso, User>{
        return parent(\.creadorID)
    }
    
    var categorias:Siblings<Recurso,Categoria, RecursoCategoriaPivot>{
        return siblings()
    }
    
}


extension Recurso {
    static var createdAtKey: TimestampKey {
        return \.creado
    }
    static var updatedAtKey: TimestampKey {
        return \.actualizado
    }
}


extension Recurso:Model{
    typealias Database = MySQLDatabase
    typealias ID = UUID
    static var idKey:IDKey{ return \.id}
}

extension Recurso:Migration{
    static func prepare(on connection: Database.Connection) -> Future<Void>{
        return Database.create(Recurso.self, on: connection, closure: { (builder) in
            try addProperties(to: builder)
        })
    }
}


extension Recurso:Content{}
extension Recurso:Parameter{}
