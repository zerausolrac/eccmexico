import Vapor
import FluentMySQL

final class FavoritoPivot:MySQLUUIDPivot{
    static var leftIDKey: WritableKeyPath<FavoritoPivot, Int> {return \.username}
    static var rightIDKey: WritableKeyPath<FavoritoPivot, UUID> { return \.recursoID}
    
    var id:UUID?
    typealias Left = User
    typealias Right = Recurso
    
    var username:User.ID
    var recursoID:Recurso.ID
    
    init(_ username:User.ID, _ recursoID:Recurso.ID) {
        self.username = username
        self.recursoID = recursoID
    }
}


extension FavoritoPivot:Migration {}
