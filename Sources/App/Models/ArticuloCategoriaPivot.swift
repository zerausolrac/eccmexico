import Vapor
import FluentMySQL


final class ArticuloCategoriaPivot:MySQLUUIDPivot, ModifiablePivot {
    
    var id: UUID?
    var articuloID:Articulo.ID
    var categoriaID:Categoria.ID
    
    typealias Left = Articulo
    typealias Right = Categoria
    
    static var leftIDKey: LeftIDKey { return \.articuloID }
    static var rightIDKey: RightIDKey { return \.categoriaID }
    
    init(_ articulo:Articulo, _ categoria:Categoria) throws {
        self.articuloID = try articulo.requireID()
        self.categoriaID = try categoria.requireID()
    }
    
}


extension ArticuloCategoriaPivot:Migration{}
