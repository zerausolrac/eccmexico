import Vapor
import FluentMySQL

final class RecursoCategoriaPivot: MySQLUUIDPivot{
    
    typealias Left = Recurso
    typealias Right = Categoria
    
    var id: UUID?
    var recursoID:Recurso.ID
    var categoriaID:Categoria.ID
    
    static var leftIDKey: WritableKeyPath<RecursoCategoriaPivot, UUID> { return \.recursoID }
    static var rightIDKey: WritableKeyPath<RecursoCategoriaPivot, Int> {return \.categoriaID }
    
    init(recursoID:Recurso.ID, categoriaID:Categoria.ID) {
        self.recursoID = recursoID
        self.categoriaID = categoriaID
    }
}

extension RecursoCategoriaPivot:Migration{}
