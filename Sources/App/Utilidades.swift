import Foundation


extension String {
    
    func buscarImagenesHTML(patron:String)-> [String]{
        let patronABuscar = "<img\\s[^>]*src=\\\"([^\"]*)\"[^>]*"
        let regex = try! NSRegularExpression(pattern: patronABuscar, options: NSRegularExpression.Options.caseInsensitive)
        let range = NSMakeRange(self.utf16.startIndex.encodedOffset, self.utf16.endIndex.encodedOffset)
        
        let resultado = regex.matches(in: self, options: NSRegularExpression.MatchingOptions(), range: range)
        
        let urls = resultado.map({encontrado -> String in
            let rangoURL = encontrado.range(at: 1)
            let startIdx = String.UTF16Index.init(encodedOffset: (rangoURL.location))
            let endIdx = String.UTF16Index(encodedOffset: rangoURL.length + rangoURL.length)
            return String(self[startIdx..<endIdx])
        })
      return urls
    }
    
    func buscarPrimeraImagenHTML()-> String? {
        let patronABuscar = "<img\\s[^>]*src=\\\"([^\"]*)\"[^>]*"
        let regex = try! NSRegularExpression(pattern: patronABuscar, options: NSRegularExpression.Options.caseInsensitive)
        let range = NSMakeRange(self.utf16.startIndex.encodedOffset, self.utf16.endIndex.encodedOffset)
        
        let resultado = regex.matches(in: self, options: NSRegularExpression.MatchingOptions(), range: range)
        
        let urls = resultado.map({encontrado -> String in
            let rangoURL = encontrado.range(at: 1)
            let startIdx = String.UTF16Index.init(encodedOffset: (rangoURL.location))
            let endIdx = String.UTF16Index(encodedOffset: rangoURL.location + rangoURL.length)
            return String(self[startIdx..<endIdx])
        })
        return urls.first
    }
    
    
    
    func extraerTexto()-> String {
        let patronABuscar = "^(.{0,200}(?!\\w))"
        let regex = try! NSRegularExpression(pattern: patronABuscar, options: NSRegularExpression.Options.caseInsensitive)
        let range = NSMakeRange(self.utf16.startIndex.encodedOffset, self.utf16.endIndex.encodedOffset)
        
        let resultado = regex.matches(in: self, options: NSRegularExpression.MatchingOptions(), range: range)
        
        let urls = resultado.map({encontrado -> String in
            let rangoURL = encontrado.range(at: 1)
            let startIdx = String.UTF16Index.init(encodedOffset: (rangoURL.location))
            let endIdx = String.UTF16Index(encodedOffset: rangoURL.location + rangoURL.length)
            return String(self[startIdx..<endIdx])
        })
        return urls.first!
    }
}


