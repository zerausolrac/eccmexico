import FluentMySQL
import Vapor
import Authentication
import Leaf
import Mailgun
import S3


public func configure(
    _ config: inout Config,
    _ env: inout Environment,
    _ services: inout Services
) throws {
    /// Register providers first
    try services.register(FluentMySQLProvider())
    try services.register(AuthenticationProvider())
    try services.register(LeafProvider())
    
    //Amazon S3 Service registration
    let s3AccessKey = "AKIAI6XVZDOGVVXWYSSA"
    let s3SecretKey = "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"
    let s3Config = S3Signer.Config(accessKey:s3AccessKey , secretKey: s3SecretKey, region: Region.usWest1)
    try services.register(s3:s3Config, defaultBucket: "mexicocancode")
    
    
    /// Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    /// Register middleware
    
    //Mailgun provider
    let mailgun = Mailgun(apiKey: "key-5344ba07ba9b2915fcb7ce8768c37961", domain: "mexicocancode.org")
    services.register(mailgun, as: Mailgun.self)
    
    //Session
    config.prefer(MemoryKeyedCache.self, for: KeyedCache.self)
    
    
    
    //LeafFender
    config.prefer(LeafRenderer.self, for: ViewRenderer.self)
    
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(SessionsMiddleware.self)
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)
    
    
    
    // Configure a MySQL database
   
    let hostname = Environment.get("DATABASE_HOSTNAME") ?? "localhost"
    let username = Environment.get("DATABASE_USER") ?? "root"
    let password = Environment.get("DATABASE_PASSWORD") ?? "eccmexico"
    let database = Environment.get("DATABASE_DB") ?? "eccmexico"
    
    let configuracion = MySQLDatabaseConfig(hostname: hostname, port: 3306, username: username, password: password, database: database)
    let miDatabase = MySQLDatabase(config: configuracion)
    var databaseConfig = DatabasesConfig()
    databaseConfig.add(database: miDatabase, as: .eccdatabase)
    services.register(databaseConfig)
    

    /// Configure migrations
    var migrations = MigrationConfig()
    migrations.add(model: User.self, database: .eccdatabase)
    migrations.add(model: Token.self, database: .eccdatabase)
    migrations.add(model: Categoria.self, database: .eccdatabase)
    migrations.add(model: Recurso.self, database: .eccdatabase)
    migrations.add(model:Articulo.self, database: .eccdatabase)
    migrations.add(model:FavoritoPivot.self, database: .eccdatabase)
    migrations.add(model:RecursoCategoriaPivot.self, database: .eccdatabase)
    migrations.add(model: ArticuloCategoriaPivot.self, database: .eccdatabase)
    migrations.add(model: Registro.self, database: .eccdatabase)
    services.register(migrations)
}


extension DatabaseIdentifier{
    static var eccdatabase:DatabaseIdentifier<MySQLDatabase> { return .init("eccmexico")}
}



