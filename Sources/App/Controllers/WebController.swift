import Vapor
import Leaf
import S3
import FluentMySQL

import Authentication
import Crypto
import Random
import Mailgun


let recursoRuta = "/mx/recurso"
let endpontSeguro = "/mx/welcome"
let rutaArticulo = "mx/art"


final class WebController:RouteCollection{
    func boot(router: Router) throws {
       
        //Seguridad
        let redirectMW = RedirectMiddleware(A: User.self, path: "/login")
        let guardMW = User.guardAuthMiddleware()
        
        
        let authSessionRoutes = router.grouped(User.authSessionsMiddleware())
        
        //PUBLIC
        
        
        
        
        authSessionRoutes.get("/", use:home)
        authSessionRoutes.group("register") { (ruta) in
            ruta.post(User.self, use: register)
            ruta.get( use: registerGet)
        }
        authSessionRoutes.get("activate", String.parameter, use:activate)
        authSessionRoutes.get("activacion", use:activateGet)
        authSessionRoutes.get("message", Int.parameter, use:messageView)
        authSessionRoutes.get("logout", use: logout)
        authSessionRoutes.get("login", use:loginGet)
        authSessionRoutes.group("login") {(ruta) in
            ruta.post(User.self, use:login)
        }
        authSessionRoutes.group("passwordreset") { ruta in
            ruta.get("/", use:passwordResetGet)
            ruta.post(User.self, use:passwordReset)
        }
        
        
        
        //Add recurso to favorito Pivot
        authSessionRoutes.post("mx","recurso","favorito",Recurso.parameter,use:setFavorito)
        authSessionRoutes.get("mx","recurso","getfavorito", Recurso.parameter,use:getFavorito)
        
        //AWS S3
        authSessionRoutes.get(recursoRuta,"all", use:allRecursos)
        authSessionRoutes.get(recursoRuta,"alls3", use:getAllResources)
        
        
        //Articulo
        authSessionRoutes.get("mx","art", "verarticulo", String.parameter, use:mostrarArticulo)
        authSessionRoutes.get("mx","art", "verarticulos", use:mostrarTodosArticulos)
        
        
        //Perfil
        authSessionRoutes.get("perfil", use:pefilGet)
        authSessionRoutes.post(PerfilUpdate.self, at: "perfilupdate",  use: perfilUpdate)
        
        
        
        
        //PROTECTED; the order of middleware is IMPORTANT!
        let protectedRute = authSessionRoutes.grouped([redirectMW,guardMW])
        
        protectedRute.group("mx") { (ruta) in
            ruta.get( use:welcome)
            ruta.get("recurso","video", String.parameter , use:verVideo)
            ruta.post(Recurso.self, at: "recurso","add", use: newRecurso)
            
            //All categories for recurso.leaf
            ruta.get("recurso", use:getAllCategoriasView)
            
            
            //Add categoria to Recurso
            ruta.post("recurso",Recurso.parameter, "categoria", Categoria.parameter, use:addCategorias)
            //Categorias of Recurso
            ruta.get("recurso", Recurso.parameter,"categorias", use:getAllCategorias)
            //All Recursos that belong to Categoria
            ruta.get("recurso","categoria", Categoria.parameter, use: getAllRecursosDeCategoriaView)
        
        
            //AWS S3
            ruta.get("upload", use:uploadFileGet)
            ruta.post("upload", use:uploadFile)
            ruta.post("perfil", use:uploadPerfilTest)
            ruta.post("uploadimagen", use:uploadImage)
            
            //Articulo
            ruta.post( CrearArticuloConCategorias.self, at: "art","crearticulo", use:crearArticulo)
            ruta.get("art","articulo", use:articuloGet)
        }
        
        
        protectedRute.group("users") { (ruta) in
            ruta.get("all", use:all)
        }
        
     
        
    }
    
    
    
    
    
    
    
    
// MARK: - USUARIO
    
    func register(_ req:Request, usuarioSolicitado:User) throws -> Future<Response>{
        do {
            try usuarioSolicitado.validate()
            
            
            return  User.query(on: req).filter(\User.username == usuarioSolicitado.username).first().flatMap(to: Response.self, { existeUsuario  in
                guard let _ = existeUsuario else {
                    let hasher = try req.make(BCryptDigest.self)
                    usuarioSolicitado.password = try hasher.hash(usuarioSolicitado.password, cost: 4)
                    
                    //temporary token generator
                    let activationToken = try CryptoRandom().generateData(count: 16).base64EncodedString()
                    //Send email notification to validate user
                    let urlActivacion = try self.urlTokenActivacion(token: activationToken, username: usuarioSolicitado.username)
                    let emailMensaje = EmailMensaje(to: usuarioSolicitado.username,
                                                    cc: nil,
                                                    bcc: nil,
                                                    subject: "Validación cuenta MexicoCanCode",
                                                    text: "",
                                                    html: "Activar cuenta dando click en: <br><br><br><a href='\(urlActivacion)'>\(urlActivacion)</a> <br><br><b>Equipo MexicoCanCode</b>")
                    
                    
                    let _ = try self.envioEmail(req, token: activationToken, mensaje:emailMensaje)
                    
                    usuarioSolicitado.tokenActivacion = activationToken
                    usuarioSolicitado.usuarioActivo = false
                    usuarioSolicitado.role = Roles.none.rawValue
                    
                    return usuarioSolicitado.save(on: req).map(to: Response.self, { (usuario)  in
                        return req.redirect(to: "/message/" + "1")
                    })
                }
                //throw Abort(HTTPResponseStatus.badRequest, reason: "User exist")
                return Future.map(on: req, { req.redirect(to: "/message/" + "2")})
            })
        } catch {
            //throw Abort(HTTPResponseStatus.badRequest, reason: "input data missing")
            return Future.map(on: req, { req.redirect(to: "/message/" + "3")})
        }
        
    }
    
    
    
    func registerGet(_ req:Request) throws -> Future<View>{
        let view = try req.make(LeafRenderer.self)
        let contexto = [String:String]()
        return view.render("registro", contexto)
    }
    
    
    
    
    
    func activate(_ req:Request) throws -> Future<Response>{
        let tokenData =  Data(base64Encoded: try req.parameters.next(String.self))!
        //decode URL
        let decoder = JSONDecoder()
        let json = try decoder.decode(EmailToken.self, from: tokenData)
        return  User.query(on: req).filter(\User.username == json.email).first().map(to: Response.self, { (usuario)  in
            guard let usuario = usuario else {
                throw Abort(HTTPResponseStatus.badRequest, reason: "Error activaion")
            }
            if json.token == usuario.tokenActivacion!  {
                usuario.tokenActivacion = "none"
                usuario.usuarioActivo = true
                let _ = usuario.update(on: req)
                return req.redirect(to: "/activacion")
            }
            
            return req.redirect(to: "/message/" + "6")
        })
    }
    
    func activateGet(_ req:Request) throws -> Future<View>{
        let view = try req.make(LeafRenderer.self)
        let contexto = [String:String]()
        return view.render("activacion", contexto)
    }
    
    
    
    
    
    func passwordReset(_ req:Request, usuario:User) throws -> Future<View>{
        return  User.query(on: req).filter(\User.username == usuario.username).first().flatMap(to: View.self, { user  in
            guard let usuarioExiste = user else {
                return try self.customView(req, leafPath: "passwordreset", values: ["error" : "usuario no existe"])
            }
            //user new token generarion
            let activationToken = try CryptoRandom().generateData(count: 16).base64EncodedString()
            
            let urlActivacion = try self.urlTokenActivacion(token: activationToken, username: usuarioExiste.username)
            
            let mensajeEmail = EmailMensaje(to: usuarioExiste.username,
                                            cc: nil,
                                            bcc: nil,
                                            subject: "Re-activacion contraseña",
                                            text: "Mexico Can Code",
                                            html: "Activar cuenta dando click en: <br><br><br><a href='\(urlActivacion)'>\(urlActivacion)</a> <br><br><b>Equipo MexicoCanCode</b>")
            
            //user new password Encrypt
            let hasher =  try req.make(BCryptDigest.self)
            usuarioExiste.password = try hasher.hash(usuario.password, cost: 4)
            usuarioExiste.tokenActivacion = activationToken
            usuarioExiste.usuarioActivo = false
            let _ = try self.envioEmail(req, token: activationToken, mensaje: mensajeEmail)
            let _ = usuarioExiste.save(on: req)
            return try self.customView(req, leafPath: "error", values: ["causa":"Envio a \(usuarioExiste.username) para cambio de contraseña"])
        })
        
    }
    
    func passwordResetGet(_ req:Request) throws -> Future<View>{
        return try customView(req, leafPath: "passwordreset", values: [:])
    }
    
    
    
    
    
    
    
    
    
    func login(_ req:Request, usuarioSolicitado:User) throws -> Future<View>{
        do {
            try usuarioSolicitado.validate()
            
            let verificador = try req.make(BCryptDigest.self)
            return User.authenticate(username: usuarioSolicitado.username, password: usuarioSolicitado.password, using: verificador, on: req).flatMap(to: View.self, { usuarioAutenticado in
                guard let usuarioAutenticado = usuarioAutenticado else {
                    return try self.customView(req, leafPath: "login", values: ["error":"Password/Username incorrect!"])
                }
                //Check if user is active
                if usuarioAutenticado.usuarioActivo == true {
                    
                    try req.authenticate(usuarioAutenticado)
                    try req.authenticateSession(usuarioAutenticado)
                    
                    //SessionID
                    let currentSession = try req.session()
                    //Cookie Name
                    currentSession["id"] = String(usuarioAutenticado.id!)
                    currentSession["name"] = usuarioAutenticado.name!
                    currentSession["role"] = String(usuarioAutenticado.role!)
                    currentSession["username"] = usuarioAutenticado.username
                    print("currentSession:\(currentSession.data) user: \(usuarioAutenticado)")
                    
                    return try self.customView(req, leafPath: "welcome", values: ["name":currentSession["name"]!,"role":currentSession["role"]!])
                    
                } else {
                    let error = "Necsitas activar tu cuenta!"
                    return try self.customView(req, leafPath: "login", values: ["error":error])
                }
            })
        } catch(let error) {
            let error = BasicValidationError(error.localizedDescription)
            return try self.customView(req, leafPath: "login", values: ["error":error.message])
            
        }
    }
    
    
    
    
    func loginGet(_ req:Request) throws ->  Future<View>{
        let view = try req.make(LeafRenderer.self)
        let contexto = [String:String]()
        return view.render("login",contexto)
    }
    
    
    
    //Profile
    func pefilGet(_ req:Request) throws -> Future<View>{
        return try customView(req, leafPath: "perfil", values: [ : ])
    }
    
    
    
    //Profile Post
    func perfilUpdate(_ req:Request, perfilData:PerfilUpdate) throws -> Future<View>{
        let s3 = try req.makeS3Client()
        //Identify Data, file name
        let filext = String(perfilData.filename.split(separator: ".")[1])
        let filename = try req.session().id!
        
        let mimeType:MimeExtension = extensionValido(ext: filext)

        let archivo = File.Upload(data: perfilData.avatar, bucket: "mexicocancode", destination: filename + "." + filext, access: .publicRead, mime: "image/" + mimeType.rawValue)
        

        
        return try s3.put(file: archivo, headers: [ : ], on: req).flatMap(to: View.self, { archivoSubido  in
            
            
            let respuesta = ImagenRespuesta(uploaded: 1, fileName:archivoSubido.path, url:"https://s3-us-west-1.amazonaws.com/mexicocancode/" + archivoSubido.path)
            
            
            return try req.view().render("validarperfil", respuesta)
        })
    }
    
    
    
    struct PerfilUpdate:Content{
        var filename:String
        var avatar:Data
    }
    enum MimeExtension:String{
        case jpg
        case png
        
    }
    
    func extensionValido(ext:String) -> MimeExtension {
        switch ext.lowercased(){
        case "jpg":
           return  .jpg
        case "jpeg":
            return .jpg
        case "png":
            return .png
        default:
            return .jpg
        }
    }
    
    
    
    
    func all(_ req:Request)throws -> Future<[User]>{
        return User.query(on: req).all()
    }
    
    
    func home(_ req:Request) throws -> Future<View>{
        let view = try req.make(LeafRenderer.self)
        let contexto = [String:String]()
        return view.render("home", contexto)
    }
    
    func logout(_ req:Request)throws -> Response {
        try req.unauthenticateSession(User.self)
        try req.destroySession()
        return req.redirect(to: "/")
    }
    
    
    //Protected
    func welcome(_ req: Request) throws -> Future<View>{
        return try self.customView(req, leafPath: "welcome", values: [:])
    }
    
    
    
    //Utilities
    
    
    //Cookie
    typealias Key = Llaves
    enum Llaves:CodingKey{
        case name
        case email
    }
    
    struct Propidades:Codable{
        var name:String
        init(name:String) {
            self.name = name
        }
    }
    
    
    //Custom View
    func messageView(_ req:Request) throws -> Future<View>{
        let view = try req.make(LeafRenderer.self)
        let errorId = try req.parameters.next(Int.self)
        var contexto = [String:String]()
        switch errorId{
        case 0:
            contexto = ["causa":"URL invalida"]
        case 1:
            contexto = ["causa":"Se ha creado tu cuenta, revisa tu email para activar"]
        case 2:
            contexto = ["causa":"Existe usuario"]
        case 3:
            contexto = ["causa":"Faltan datos"]
            
            
        default:
            contexto = ["causa": "Error desconocido"]
        }
        return view.render("error", contexto)
    }
    
    
    func customView(_ req: Request, leafPath:String, values:[String:String])throws -> Future<View> {
        let view = try req.make(LeafRenderer.self)
        let sesion = try req.session()
        var context = [String:String]()
        if sesion.id != nil {
            context["name"] = sesion["name"]
            context["role"] = sesion["role"]
        }
        values.forEach { (key,value) in
            context[key] = value
        }
        
        return view.render(leafPath, context)
    }
    
    
    
    //Gunmail
    struct EmailMensaje {
        var from:String = "postmaster@mexicocancode.org"
        var to: String
        var cc: String?
        var bcc: String?
        var subject: String
        var text: String
        var html: String?
        
        init(to:String, cc:String?, bcc:String?, subject:String, text: String, html: String?) {
            self.to = to
            self.cc = cc
            self.bcc = bcc
            self.subject = subject
            self.text = text
            self.html = html
        }
    }
    
    
    func envioEmail(_ req:Request, token:String, mensaje:EmailMensaje) throws ->  Future<Response>{
        //3 Mailgun Sevice
        let mensaje = Mailgun.Message(
            from: mensaje.from,
            to: mensaje.to,
            cc: mensaje.cc,
            bcc: mensaje.bcc,
            subject: mensaje.subject,
            text: mensaje.text,
            html: mensaje.html
        )
        
        let mailgun = try req.make(Mailgun.self)
        return try mailgun.send(mensaje, on: req)
        
    }
    
    
    
    func urlTokenActivacion(token:String, username:String )throws -> String{
        //1 Encode json to URL
        let mailToken = EmailToken(email: username, token: token)
        let encoder = JSONEncoder()
        let tokenEmailEncoded = try encoder.encode(mailToken).base64EncodedString()
        
        //2 Build URL
        let seccionesUrl = ["activate", tokenEmailEncoded]
        let url = urlRutas(ruta: "http://mexicocancode.org", secciones: seccionesUrl)?.absoluteString
        return url!
    }
    
    
    
    func urlCompuesta(componentes:[String:String],  urlBase:URL) -> URL? {
        var items = URLComponents.init(url: urlBase, resolvingAgainstBaseURL: true)
        items?.queryItems = componentes.compactMap({ (llave,valor) -> URLQueryItem in
            URLQueryItem(name: llave, value: valor)
        })
        return items?.url
    }
    
    
    func urlRutas(ruta:String, secciones:[String]) -> URL?{
        var baseURL = URL(string:ruta)
        baseURL = secciones.reduce(baseURL) { (baseURL, seccion) -> URL? in
            baseURL?.appendingPathComponent(seccion)
        }
        return baseURL
    }
    
    
    
    
   // MARK: - RECURSO
    
    func newRecurso(_ req:Request, recurso:Recurso) throws -> Future<HTTPResponse>{
        return recurso.save(on: req).transform(to: HTTPResponse(status: .ok))
    }
    
    func allRecursos(_ req: Request) throws -> Future<[Recurso]>{
        return  Recurso.query(on: req).all()
    }
    
    
    //Video player for recurso ID
    func verVideo( _ req:Request) throws -> Future<View>{
        let param1 = try req.parameters.next(String.self)
        //let id = UUID.init(param1)
        let id = UUID(param1)
        return  Recurso.query(on: req).filter(\Recurso.id == id!).first().flatMap(to: View.self, { (recurso) in
            guard let recurso = recurso else {
                throw VaporError(identifier: "001", reason: "Error no encontrado")
            }
            
            //  mx/recurso/{id_recurso}/user/{id_user}
            let recurso_id =  recurso.id!.uuidString
            let contexto = FavoritoContexto(rid: recurso_id, urlVideo: recurso.urlStorage)
            return try req.view().render("video", contexto)
        })
    }
    
    
    
    
    func setFavorito(_ req:Request) throws -> Future<String>{
        let user_id =  Int(try req.session()["id"]!)
        let usuario =  User.query(on: req).filter(\User.id  == user_id).first().map(to: User.self) { (usuario) in
            return usuario!
        }
        return flatMap(to: String.self,try req.parameters.next(Recurso.self) ,usuario, { (recurso, usuario) in
            return FavoritoPivot.query(on: req).group(.and) { builder in
                builder.filter(\FavoritoPivot.username == usuario.id!)
                builder.filter(\FavoritoPivot.recursoID == recurso.id!)
                }.first().map(to: String.self, { existeFavorito in
                    guard let existeFavorito = existeFavorito else {
                        let addFavorito = try FavoritoPivot(usuario.requireID(), recurso.requireID())
                        let  _ = addFavorito.save(on: req)
                        return "on"
                    }
                    let _ = existeFavorito.delete(on: req)
                    return "off"
                })
        })
        
    }
    
    
    
    func getFavorito(_ req:Request) throws -> Future<String>{
        let user_id =  Int(try req.session()["id"]!)
        let usuario =  User.query(on: req).filter(\User.id  == user_id).first().map(to: User.self) { (usuario) in
            return usuario!
         }
        return flatMap(to: String.self, try req.parameters.next(Recurso.self), usuario, { (recurso, usuario)  in
            
            return FavoritoPivot.query(on: req).group(MySQLBinaryOperator.and, closure: { (pivote) in
                pivote.filter(\FavoritoPivot.username == usuario.id!)
                pivote.filter(\FavoritoPivot.recursoID == recurso.id!)
            }).first().map(to: String.self, { pivoteResultado in
                guard pivoteResultado != nil else {
                    return "off"
                }
                return "on"
            })
            
        })
        
    }
    
    
    
    
    
    //AWS S3
    func getAllResources(_ req:Request) throws -> Future<BucketsInfo>{
        let s3 = try req.makeS3Client()
        return try s3.buckets(on: req)
        //        return try s3.list(bucket: "mexicocancode", region: .usWest1, headers: [:], on: req).catchMap({ (error) -> (BucketResults) in
        //            if let error = error.s3ErroMessage() {
        //                print(error.message)
        //            }
        //            throw error
        //        })
        
    }
    
    
    
    
    func uploadFile(_ req:Request) throws -> Future<HTTPResponse>{
        let s3 = try req.makeS3Client()
        return try req.content.decode(UploadRecurso.self).flatMap({ uploadArchivo in
            let file = File.Upload(data: uploadArchivo.archivo, bucket: nil, destination: "imagen.jpg", access: AccessControlList.publicRead, mime: "image/jpeg")
            return try s3.put(file: file, headers: [ : ], on: req).flatMap(to: HTTPResponse.self, { (file)  in
                return Future.map(on: req, {
                    return HTTPResponse(status:.ok)
                })
            })
        })
    }
    
    
    
    
    
    
    //SUBIR IMAGEN A AMAZON S3
    
    struct UploadImagenRequest:Content {
        var upload:Data //or File if want to access file's name
    }
    
    struct ImagenRespuesta:Content{
        //Expect response for CKEditor & CKFinder
        var uploaded:Int
        var fileName:String
        var url:String
        //var error:String
    }
    
    
    enum FormatoImagen{
        case jpg
        case png
    }
    
    func uploadImage(_ req:Request) throws -> Future<ImagenRespuesta>{
        let s3 = try req.makeS3Client()
        
        return try req.content.decode(UploadImagenRequest.self).flatMap({ uploadArchivo in
            //identify Data (image) extension or mimeType
            
            let file = File.Upload(data: uploadArchivo.upload, bucket: "mexicocancode", destination: "imagen04.jpg", access: AccessControlList.publicRead, mime: "image/jpeg")
            
            //identify Image exits
            return try self.existeImagenBucket(req, nombreImagen: file.path).flatMap({ existe in
                if existe == true {
                    return Future.map(on:req,{
                        let  respuesta = ImagenRespuesta(uploaded: 0, fileName: "", url: "")
                        // let  respuesta = ImagenRespuesta(uploaded: 0, fileName: "", url: "", error: "message:\"existe\"")
                        return respuesta
                        
                    })
                    
                } else {
                    
                    return try s3.put(file: file, on: req)
                        .flatMap(to: ImagenRespuesta.self, { archivoSubido in
                            return Future.map(on: req, {
                                let respuesta = ImagenRespuesta(uploaded: 1, fileName:archivoSubido.path, url:"https://s3-us-west-1.amazonaws.com/mexicocancode/" + archivoSubido.path)
                                print(respuesta)
                                return respuesta
                            })
                        })
                }
            })
        })
    }
    
    
    func existeImagenBucket(_ req:Request, nombreImagen:String) throws -> Future<Bool> {
        let s3 =  try req.makeS3Client()
        
        return try s3.get(fileInfo: nombreImagen, headers:[:], on: req).flatMap(to: Bool.self, { (existe)  in
            guard let etag = existe.etag   else{
                return Future.map( on:req, { return false })
            }
            print(etag)
            return Future.map( on:req, { return true  })
        }).catchMap({ (error)  in
            print(error.localizedDescription)
            return  false
        })
        
    }
    
    
    
    
    
    
    
    
    //SUBIR IMAGEN PERFIL
    
    func uploadPerfilTest(_ req: Request) throws -> Future<HTTPResponse>{
        return try req.content.decode(UploadPerfil.self).map(to: HTTPResponse.self, { perfil  in
            print(perfil)
            return HTTPResponse(status:.ok)
        })
    }
    
    
    
    func uploadFileGet(_ req: Request) throws -> Future<View>{
        return try req.view().render("upload")
    }
    
    
    
    
    //Relationship
    
    func addCategorias(_ req:Request) throws -> Future<HTTPStatus>{
        return flatMap(to: HTTPStatus.self, try req.parameters.next(Recurso.self), try req.parameters.next(Categoria.self), { (recurso, categoria) in
            
            let pivot = try RecursoCategoriaPivot(recursoID: recurso.requireID(), categoriaID: categoria.requireID())
            
            return pivot.save(on: req).transform(to: .created)
        })
    }
    
    
    
    func getAllCategorias(_ req:Request) throws -> Future<[Categoria]>{
        return try req.parameters.next(Recurso.self).flatMap(to: [Categoria].self, { (recurso) in
            return try recurso.categorias.query(on: req).all()
        })
    }
    
    
    
    func getAllCategoriasView(_ req: Request) throws -> Future<View>{
        let categorias = Categoria.query(on:req).all()
        let contexto =  AllCategoriaContexto(categorias: categorias)
        return try req.view().render( "recursos", contexto)
    }
    
    
    
    func getAllRecursosDeCategoriaView(_ req: Request) throws -> Future<View>{
        let recursos = try req.parameters.next(Categoria.self).flatMap(to: [Recurso].self, { categoria in
            return try categoria.recursos.query(on: req).all()
        })
        
        let sesion = try req.session()
        if sesion.id != nil {
            let contexto = AllRecursoContexto(name: sesion["name"]!, role: sesion["role"]!, recursos: recursos)
            return try req.view().render("categoria", contexto)
        }
        return try req.view().render("login")
    }
    
    
    
// MARK: - ARTICULO
    
    
    func crearArticulo(_ req:Request, data:CrearArticuloConCategorias) throws -> Future<HTTPResponse>{
        
        //Create Articulo
        let articulo = Articulo(titulo: data.titulo, cuerpo: data.cuerpo, visible: data.visible, autor: data.autor)
        return articulo.save(on: req).flatMap(to: HTTPResponse.self, { (articuloSalvado)  in
            var categoriasSalvadas: [Future<Void>] = []
            
            try data.categorias?.forEach({ nombreCategoria in
                try categoriasSalvadas.append(self.salvarArticuloCategoriasPivote(articulo: articuloSalvado, nombreCategoria: nombreCategoria, on: req))
                
            })
            print("categoriasSalvadas: \(categoriasSalvadas)")
            
            return categoriasSalvadas.flatten(on: req).transform(to:HTTPResponse(status: HTTPResponseStatus.ok) )
            
            
        })
        
    }
    
    
    func salvarArticuloCategoriasPivote( articulo:Articulo, nombreCategoria:String, on req:Request) throws -> Future<Void> {
        return Categoria.query(on: req).filter(\.nombreCategoria == nombreCategoria).first().flatMap(to: Void.self, { existeCategoria  in
            if let laCategoria = existeCategoria {
                return articulo.articuloCategorias.attach(laCategoria, on: req).transform(to: ())
            } else {
                return Future.map(on:req, {return })
            }
        })
        
        
    }
    
    
    //Present content of a single Articulo
    func mostrarArticulo(_ req:Request) throws -> Future<View> {
        let articuloID = try req.parameters.next(String.self)
        return Articulo.query(on: req).filter(\.id == UUID(articuloID)).first().flatMap(to: View.self) { (existe)  in
            guard let articulo = existe else{
                throw VaporError(identifier: "000", reason: "No existe articulo")
            }
            let contexto = MostrarArticuloContexto(articulo: articulo)
            print(contexto)
            return try req.view().render("verarticulo", contexto)
        }
        
    }
    
    
    
    
    
    
    //Present list of All Articulo
    func mostrarTodosArticulos(_ req:Request) throws -> Future<View>{
        var articulosWeb:[ArticuloWeb] = []
        
        //NSAttributedString
        return Articulo.query(on: req).all().flatMap(to: View.self, { articulos in
            articulos.forEach({ articulo in
                if let imagenEncontrada = articulo.cuerpo.buscarPrimeraImagenHTML(){
                    let articuloWeb = ArticuloWeb(
                        titulo:articulo.titulo,
                        imagen: imagenEncontrada,
                        texto: articulo.cuerpo.extraerTexto(),
                        idArticulo: articulo.id!.uuidString)
                    articulosWeb.append(articuloWeb)
                } else {
                    let articuloWeb = ArticuloWeb(
                        titulo:articulo.titulo,
                        imagen: "No imagen",
                        texto: articulo.cuerpo.extraerTexto(),
                        idArticulo: articulo.id!.uuidString)
                    articulosWeb.append(articuloWeb)
                }
                
                
            })
            let contexto = MostrarTodosArticulosContexto(articulos: articulosWeb)
            return try req.view().render("verarticulos", contexto)
        })
    }
    
    //Edit/Update Articulo
    
    
    
    
    //Delete Articulo
    
    
    
    
    func articuloGet(_ req: Request) throws -> Future<View> {
        var autor_id:Int
        let categorias = Categoria.query(on: req).all()
        if let user_id =  try req.session()["id"] {
            autor_id = Int(user_id)!
        }else{
            autor_id = Int("1")!
        }
        let contexto = AllCategoriasContexto(categorias: categorias, autor: autor_id)
        return try req.view().render("articulo", contexto)
    }
    
    
    
    
    
    
    
    
    
}

//USUARIO UTILIDADES
struct EmailToken:Codable,CustomStringConvertible{
    var description: String{
        return "email:\(email) token:\(token)"
    }
    var email:String
    var token:String
}


//Personalized return of error message to the same requesting page
extension Request {
    func errorLogin(ruta:String, razon:String) throws -> Future<Response> {
        let res = makeResponse()
        let contexto = ["error":razon]
        return try res.make(LeafRenderer.self).render(ruta, contexto).map(to: Response.self) { vista in
            res.http.body = HTTPBody(data: vista.data)
            return res
        }
    }
}





//Structure as context 
struct UploadRecurso:Content, CustomStringConvertible {
    var description: String {return "titulo:\(titulo) descripcion:\(descripcion) archivo:\(archivo)" }
    var titulo:String
    var descripcion:String
    var archivo:Data //or File if want to access file's name
}


struct UploadPerfil:Content, CustomStringConvertible{
    var description: String { return "nombre:\(nombre) avatar:\(avatar)"}
    var nombre:String
    var avatar:Data
}


protocol Sesionable{
    var name:String {get set}
    var role:String {get set}
}

struct AllCategoriaContexto: Encodable{
    var categorias: Future<[Categoria]>
}

struct AllRecursoContexto: Encodable, Sesionable{
    var name: String
    var role: String
    var recursos: Future<[Recurso]>
}


struct FavoritoContexto:Encodable{
    let rid: String
    let urlVideo:String
}

struct GetFavoritoContexto:Codable{
    let isFavorito:String
}

//struct para contexto Articulos
struct AllCategoriasContexto: Encodable {
    let title = "All Categories"
    let categorias: Future<[Categoria]>
    let autor:User.ID
}

struct CrearArticuloConCategorias:Content {
    var titulo:String
    var cuerpo:String
    var visible:Bool
    var autor:User.ID
    var categorias:[String]?
}


struct MostrarTodosArticulosContexto:Encodable {
    let titulo = "Lista Articulos"
    var articulos:[ArticuloWeb]
}

struct ArticuloWeb:Content {
    var titulo:String
    var imagen:String
    var texto:String
    var idArticulo:String
}


struct MostrarArticuloContexto: Encodable {
    let titulo = "Ver Articulo"
    let articulo:Articulo
}

