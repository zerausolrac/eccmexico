import Vapor
import FluentMySQL
import Crypto
import Authentication

final class APIController:RouteCollection{
    func boot(router: Router) throws {
        let tokenMW = User.tokenAuthMiddleware()
        
        router.get("api", "allarticulos", use:allArticulos)
        
        router.group("api","login") { (ruta) in
            ruta.post(User.self, use:apiLogin)
        }
        
        //protected API
        let protectedRoute = router.grouped([tokenMW])
        protectedRoute.group("api") { (ruta) in
            ruta.get("all",use:allUsers)
            
        }
        
    }
    
    
    func apiLogin(_ req:Request, usuarioSolicitado:User) throws -> Future<Token>{
        let validator = try req.make(BCryptDigest.self)
        //Autentificar usuario
        return User.authenticate(username: usuarioSolicitado.username, password: usuarioSolicitado.password, using: validator, on: req).flatMap(to: Token.self, { (usuarioValido)  in
            guard let usuarioValido = usuarioValido else {
                throw VaporError(identifier: "0001", reason: "No Valido")
            }
            return Token.query(on: req).filter(\.userID == usuarioValido.id!).first().flatMap(to: Token.self, { (existeToken)  in
                guard let existeToken = existeToken else {
                    let token = try Token.generarToken(req, user: usuarioValido)
                    return token.save(on: req)
                }
                return Future.map(on: req, {   return existeToken })
            })
        })
    }
   
       
        
        
//        return User.authenticate(username: usuarioSolicitado.username, password: usuarioSolicitado.password, using: validator, on: req).flatMap(to: Token.self, { usuarioValido in
//            guard let usuarioValido = usuarioValido else {
//                throw Abort(HTTPResponseStatus.badRequest, reason: "User/Password Invalid")
//            }
//
//            //Token creation
//            let userToken = try Token.generarToken(req, user:usuarioValido)
//           // Token return
//            return userToken.save(on: req)
//        })
        
    
    
    
    
    func allUsers(_ req:Request) throws -> Future<[User]>{
        return User.query(on: req).all()
    }
    
    
    func allArticulos(_ req:Request) throws ->Future<[Articulo]>{
        return Articulo.query(on: req).all()
    }
    
    
}
