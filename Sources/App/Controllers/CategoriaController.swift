import Vapor
import FluentMySQL

final class CategoriaController:RouteCollection {
    func boot(router: Router) throws {
        
        
        router.group("categoria") { ruta in
            ruta.post(Categoria.self, use:create)
            ruta.get("all", use:getAll)
            ruta.get("get", Int.parameter, use:get)
        }
        
        router.get("categoria",Categoria.parameter, "recursos", use:allRecursos)
    }
    
    

    func create(_ req:Request, categoria:Categoria) throws -> Future<Categoria>{
        return categoria.save(on: req)
    }

    
    func getAll(_ req:Request) throws -> Future<[Categoria]>{
        return Categoria.query(on: req).all()
    }
    
    
    func get(_ req:Request) throws -> Future<Categoria>{
        let parametro = try req.parameters.next(Int.self)
        return  Categoria.query(on: req).filter(\.id == parametro).first().map(to: Categoria.self, { categoria in
            guard let categoria = categoria else {
                throw VaporError( identifier: "001", reason: "No existe categoria")
            }
            return categoria
    })
        
        //return try req.parameters.next(Categoria.self)
    }
    
    
    //Relationship
    func allRecursos(_ req: Request) throws -> Future<[Recurso]>{
        return try req.parameters.next(Categoria.self).flatMap(to: [Recurso].self, { (categoria)  in
              return try categoria.recursos.query(on: req).all()
        })
        
    }
    
    
    
}
