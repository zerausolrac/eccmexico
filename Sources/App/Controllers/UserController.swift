import Vapor
import Authentication
import FluentMySQL
import Crypto
import Leaf
import Random
import Mailgun


//let endpontSeguro = "/mx/welcome"

final class UserController:RouteCollection{
    func boot(router: Router) throws {
//        let authMW = User.authSessionsMiddleware()
//        let redirectMW = RedirectMiddleware(A: User.self, path: "/login")
//        let guardMW = User.guardAuthMiddleware()
//
//
//       //public
//        router.get("/", use:home)
//        router.group("register") { (ruta) in
//            ruta.post(User.self, use: register)
//            ruta.get("/", use: registerGet)
//        }
//        router.get("activate", String.parameter, use:activate)
//        router.get("activacion", use:activateGet)
//        router.get("message", Int.parameter, use:messageView)
//        router.get("logout", use: logout)
//        router.get("login", use:loginGet)
//        router.group("login") {(ruta) in
//            ruta.post(User.self, use:login)
//        }
//        router.group("passwordreset") { ruta in
//            ruta.get("/", use:passwordResetGet)
//            ruta.post(User.self, use:passwordReset)
//        }
//        router.get("perfil", use:pefilGet)
//
//
//
//
//
//        //protected; the order of middleware is IMPORTANT!
//        let protectedRute = router.grouped([authMW,redirectMW,guardMW])
//        protectedRute.group("mx") { (ruta) in
//            ruta.get("welcome", use:welcome)
//        }
//        protectedRute.group("users") { (ruta) in
//            ruta.get("all", use:all)
//        }
//
    }
    
    
    
    
    
//    func register(_ req:Request, usuarioSolicitado:User) throws -> Future<Response>{
//        do {
//        try usuarioSolicitado.validate()
//            
//            
//        return  User.query(on: req).filter(\User.username == usuarioSolicitado.username).first().flatMap(to: Response.self, { existeUsuario  in
//            guard let _ = existeUsuario else {
//                let hasher = try req.make(BCryptDigest.self)
//                usuarioSolicitado.password = try hasher.hash(usuarioSolicitado.password, cost: 4)
//
//                //temporary token generator
//                let activationToken = try CryptoRandom().generateData(count: 16).base64EncodedString()
//                //Send email notification to validate user
//                let urlActivacion = try self.urlTokenActivacion(token: activationToken, username: usuarioSolicitado.username)
//                let emailMensaje = EmailMensaje(to: usuarioSolicitado.username,
//                                                cc: nil,
//                                                bcc: nil,
//                                                subject: "Validación cuenta MexicoCanCode",
//                                                text: "",
//                                                html: "Activar cuenta dando click en: <br><br><br><a href='\(urlActivacion)'>\(urlActivacion)</a> <br><br><b>Equipo MexicoCanCode</b>")
//
//
//                let _ = try self.envioEmail(req, token: activationToken, mensaje:emailMensaje)
//
//                usuarioSolicitado.tokenActivacion = activationToken
//                usuarioSolicitado.usuarioActivo = false
//                usuarioSolicitado.role = Roles.none.rawValue
//
//                return usuarioSolicitado.save(on: req).map(to: Response.self, { (usuario)  in
//                    return req.redirect(to: "/message/" + "1")
//                })
//            }
//            //throw Abort(HTTPResponseStatus.badRequest, reason: "User exist")
//            return Future.map(on: req, { req.redirect(to: "/message/" + "2")})
//        })
//        } catch {
//            //throw Abort(HTTPResponseStatus.badRequest, reason: "input data missing")
//            return Future.map(on: req, { req.redirect(to: "/message/" + "3")})
//        }
//    
//    }
//    
//    
//    
//    func registerGet(_ req:Request) throws -> Future<View>{
//        let view = try req.make(LeafRenderer.self)
//        let contexto = [String:String]()
//         return view.render("registro", contexto)
//    }
//    
//    
//    
//    
//    
//    func activate(_ req:Request) throws -> Future<Response>{
//        let tokenData =  Data(base64Encoded: try req.parameters.next(String.self))!
//        //decode URL
//        let decoder = JSONDecoder()
//        let json = try decoder.decode(EmailToken.self, from: tokenData)
//        return  User.query(on: req).filter(\User.username == json.email).first().map(to: Response.self, { (usuario)  in
//            guard let usuario = usuario else {
//                throw Abort(HTTPResponseStatus.badRequest, reason: "Error activaion")
//            }
//            if json.token == usuario.tokenActivacion!  {
//                usuario.tokenActivacion = "none"
//                usuario.usuarioActivo = true
//                let _ = usuario.update(on: req)
//                return req.redirect(to: "/activacion")
//            }
//            
//            return req.redirect(to: "/message/" + "6")
//        })
//    }
//
//    func activateGet(_ req:Request) throws -> Future<View>{
//        let view = try req.make(LeafRenderer.self)
//        let contexto = [String:String]()
//        return view.render("activacion", contexto)
//    }
//    
//    
//    
//    
//    
//    func passwordReset(_ req:Request, usuario:User) throws -> Future<View>{
//        return  User.query(on: req).filter(\User.username == usuario.username).first().flatMap(to: View.self, { user  in
//            guard let usuarioExiste = user else {
//                return try self.customView(req, leafPath: "passwordreset", values: ["error" : "usuario no existe"])
//            }
//            //user new token generarion
//            let activationToken = try CryptoRandom().generateData(count: 16).base64EncodedString()
//            
//            let urlActivacion = try self.urlTokenActivacion(token: activationToken, username: usuarioExiste.username)
//            
//            let mensajeEmail = EmailMensaje(to: usuarioExiste.username,
//                                            cc: nil,
//                                            bcc: nil,
//                                            subject: "Re-activacion contraseña",
//                                            text: "Mexico Can Code",
//                                            html: "Activar cuenta dando click en: <br><br><br><a href='\(urlActivacion)'>\(urlActivacion)</a> <br><br><b>Equipo MexicoCanCode</b>")
//            
//            //user new password Encrypt
//            let hasher =  try req.make(BCryptDigest.self)
//            usuarioExiste.password = try hasher.hash(usuario.password, cost: 4)
//            usuarioExiste.tokenActivacion = activationToken
//            usuarioExiste.usuarioActivo = false
//            let _ = try self.envioEmail(req, token: activationToken, mensaje: mensajeEmail)
//            let _ = usuarioExiste.save(on: req)
//            return try self.customView(req, leafPath: "error", values: ["causa":"Envio a \(usuarioExiste.username) para cambio de contraseña"])
//        })
//        
//    }
//    
//    func passwordResetGet(_ req:Request) throws -> Future<View>{
//      return try customView(req, leafPath: "passwordreset", values: [:])
//    }
//    
//    
//    
//    
//    
//    
//
//    
//    
//    func login(_ req:Request, usuarioSolicitado:User) throws -> Future<View>{
//           do {
//                try usuarioSolicitado.validate()
//            
//                let verificador = try req.make(BCryptDigest.self)
//                return User.authenticate(username: usuarioSolicitado.username, password: usuarioSolicitado.password, using: verificador, on: req).flatMap(to: View.self, { usuarioAutenticado in
//                    guard let usuarioAutenticado = usuarioAutenticado else {
//                    return try self.customView(req, leafPath: "login", values: ["error":"Password/Username incorrect!"])
//                    }
//                    //Check if user is active
//                    if usuarioAutenticado.usuarioActivo == true {
//                        
//                        try req.authenticate(usuarioAutenticado)
//                        try req.authenticateSession(usuarioAutenticado)
//                        
//                        //SessionID
//                        let currentSession = try req.session()
//                        //Cookie Name
//                        currentSession["id"] = String(usuarioAutenticado.id!)
//                        currentSession["name"] = usuarioAutenticado.name!
//                        currentSession["role"] = String(usuarioAutenticado.role!)
//                        currentSession["username"] = usuarioAutenticado.username
//                        print("currentSession:\(currentSession.data) user: \(usuarioAutenticado)")
//                        
//                        return try self.customView(req, leafPath: "welcome", values: ["name":currentSession["name"]!,"role":currentSession["role"]!])
//                        
//                    } else {
//                        let error = "Necsitas activar tu cuenta!"
//                        return try self.customView(req, leafPath: "login", values: ["error":error])
//                    }
//                })
//            } catch(let error) {
//                let error = BasicValidationError(error.localizedDescription)
//                return try self.customView(req, leafPath: "login", values: ["error":error.message])
//                
//            }
//    }
//    
//    
//
//    
//    func loginGet(_ req:Request) throws ->  Future<View>{
//        let view = try req.make(LeafRenderer.self)
//        let contexto = [String:String]()
//         return view.render("login",contexto)
//    }
//    
//    
//    func pefilGet(_ req:Request) throws -> Future<View>{
//        return try customView(req, leafPath: "perfil", values: [ : ])
//    }
//    
//    
//
//    
//    func all(_ req:Request)throws -> Future<[User]>{
//        return User.query(on: req).all()
//    }
//    
//    
//    func home(_ req:Request) throws -> Future<View>{
//        let view = try req.make(LeafRenderer.self)
//        let contexto = [String:String]()
//        return view.render("home", contexto)
//    }
//    
//    func logout(_ req:Request)throws -> Response {
//        try req.unauthenticateSession(User.self)
//        try req.destroySession()
//        return req.redirect(to: "/")
//    }
//    
//    
//    //Protected
//    func welcome(_ req: Request) throws -> Future<View>{
//        return try self.customView(req, leafPath: "welcome", values: [:])
//    }
//    
//    
//    
//
// 
//
//    
//    
//    
//    
//    
//    
//    
//    
//    //Utilities
//
//    
//    //Cookie
//    typealias Key = Llaves
//    enum Llaves:CodingKey{
//        case name
//        case email
//    }
//    
//    struct Propidades:Codable{
//        var name:String
//        init(name:String) {
//            self.name = name
//        }
//    }
//    
//    
//    //Custom View
//    func messageView(_ req:Request) throws -> Future<View>{
//        let view = try req.make(LeafRenderer.self)
//        let errorId = try req.parameters.next(Int.self)
//        var contexto = [String:String]()
//        switch errorId{
//        case 0:
//            contexto = ["causa":"URL invalida"]
//        case 1:
//            contexto = ["causa":"Se ha creado tu cuenta, revisa tu email para activar"]
//        case 2:
//            contexto = ["causa":"Existe usuario"]
//        case 3:
//            contexto = ["causa":"Faltan datos"]
//            
//            
//        default:
//            contexto = ["causa": "Error desconocido"]
//        }
//        return view.render("error", contexto)
//    }
//    
//    
//    func customView(_ req: Request, leafPath:String, values:[String:String])throws -> Future<View> {
//        let view = try req.make(LeafRenderer.self)
//         let sesion = try req.session()
//        var context = [String:String]()
//        if sesion.id != nil {
//            context["name"] = sesion["name"]
//            context["role"] = sesion["role"]
//        }
//        values.forEach { (key,value) in
//            context[key] = value
//        }
//        
//        return view.render(leafPath, context)
//    }
//    
//   
//    
//    //Gunmail
//    struct EmailMensaje {
//        var from:String = "postmaster@mexicocancode.org"
//        var to: String
//        var cc: String?
//        var bcc: String?
//        var subject: String
//        var text: String
//        var html: String?
//        
//        init(to:String, cc:String?, bcc:String?, subject:String, text: String, html: String?) {
//            self.to = to
//            self.cc = cc
//            self.bcc = bcc
//            self.subject = subject
//            self.text = text
//            self.html = html
//        }
//    }
//    
//    
//    func envioEmail(_ req:Request, token:String, mensaje:EmailMensaje) throws ->  Future<Response>{
//        //3 Mailgun Sevice
//        let mensaje = Mailgun.Message(
//            from: mensaje.from,
//            to: mensaje.to,
//            cc: mensaje.cc,
//            bcc: mensaje.bcc,
//            subject: mensaje.subject,
//            text: mensaje.text,
//            html: mensaje.html
//        )
//        
//        let mailgun = try req.make(Mailgun.self)
//         return try mailgun.send(mensaje, on: req)
//
//    }
// 
//
//    
//    func urlTokenActivacion(token:String, username:String )throws -> String{
//        //1 Encode json to URL
//        let mailToken = EmailToken(email: username, token: token)
//        let encoder = JSONEncoder()
//        let tokenEmailEncoded = try encoder.encode(mailToken).base64EncodedString()
//        
//        //2 Build URL
//        let seccionesUrl = ["activate", tokenEmailEncoded]
//        let url = urlRutas(ruta: "http://mexicocancode.org", secciones: seccionesUrl)?.absoluteString
//        return url!
//    }
//    
//    
//    
//    func urlCompuesta(componentes:[String:String],  urlBase:URL) -> URL? {
//        var items = URLComponents.init(url: urlBase, resolvingAgainstBaseURL: true)
//        items?.queryItems = componentes.compactMap({ (llave,valor) -> URLQueryItem in
//             URLQueryItem(name: llave, value: valor)
//        })
//      return items?.url
//    }
//
//    
//    func urlRutas(ruta:String, secciones:[String]) -> URL?{
//        var baseURL = URL(string:ruta)
//        baseURL = secciones.reduce(baseURL) { (baseURL, seccion) -> URL? in
//            baseURL?.appendingPathComponent(seccion)
//        }
//      return baseURL
//    }
    
}


//struct EmailToken:Codable,CustomStringConvertible{
//    var description: String{
//        return "email:\(email) token:\(token)"
//    }
//    var email:String
//    var token:String
//}
//
//
//
////Extensions
//
////Personalized return of error message to the same requesting page
//extension Request {
//    func errorLogin(ruta:String, razon:String) throws -> Future<Response> {
//        let res = makeResponse()
//        let contexto = ["error":razon]
//        return try res.make(LeafRenderer.self).render(ruta, contexto).map(to: Response.self) { vista in
//            res.http.body = HTTPBody(data: vista.data)
//            return res
//        }
//    }
//}




