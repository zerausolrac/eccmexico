import Vapor
import FluentMySQL
import S3
import Leaf

//let recursoRuta = "/mx/recurso"


//struct UploadRecurso:Content, CustomStringConvertible {
//    var description: String {return "titulo:\(titulo) descripcion:\(descripcion) archivo:\(archivo)" }
//    var titulo:String
//    var descripcion:String
//    var archivo:Data //or File if want to access file's name
//}
//
//
//struct UploadPerfil:Content, CustomStringConvertible{
//    var description: String { return "nombre:\(nombre) avatar:\(avatar)"}
//    var nombre:String
//    var avatar:Data
//}
//
//
//protocol Sesionable{
//    var name:String {get set}
//    var role:String {get set}
//}
//
//struct AllCategoriaContexto: Encodable{
//    var categorias: Future<[Categoria]>
//}
//
//struct AllRecursoContexto: Encodable, Sesionable{
//    var name: String
//    var role: String
//    var recursos: Future<[Recurso]>
//}
//
//
//struct FavoritoContexto:Encodable{
//    let rid: String
//    let urlVideo:String
//}
//
//struct GetFavoritoContexto:Codable{
//    let isFavorito:String
//}




final class RecursoController:RouteCollection{
    
    
    func boot(router: Router) throws {
//        router.group(recursoRuta,"add"){ (ruta) in
//            ruta.post(Recurso.self, use: newRecurso)
//        }
        
        
        
//        //AWS S3
//        router.get(recursoRuta,"all", use:allRecursos)
//        router.get(recursoRuta,"alls3", use:getAllResources)
//        router.get("upload", use:uploadFileGet)
//        router.post("upload", use:uploadFile)
//        router.post("perfil", use:uploadPerfilTest)
//        router.post("uploadimagen", use:uploadImage)
//
//
//        //Video player for Recurso ID
//        router.get(recursoRuta,"video",String.parameter, use:verVideo)
//
//        //Add recurso to favorito Pivot
//        router.post(recursoRuta,"favorito",Recurso.parameter,use:setFavorito)
//        router.get(recursoRuta,"getfavorito", Recurso.parameter,use:getFavorito)
//
//
//        //Relationships
//        router.post(recursoRuta,Recurso.parameter, "categoria", Categoria.parameter, use:addCategorias)
//        //Categorias of Recurso
//        router.get(recursoRuta, Recurso.parameter,"categorias", use:getAllCategorias)
//        //All categories for recurso.leaf
//        router.get(recursoRuta, use:getAllCategoriasView)
//        //All Recurso's that belong to Categoria
//        router.get(recursoRuta,"categoria", Categoria.parameter, use: getAllRecursosDeCategoriaView)
    
    }


    
//
//    func newRecurso(_ req:Request, recurso:Recurso) throws -> Future<HTTPResponse>{
//        return recurso.save(on: req).transform(to: HTTPResponse(status: .ok))
//    }
//
//    func allRecursos(_ req: Request) throws -> Future<[Recurso]>{
//        return  Recurso.query(on: req).all()
//    }
//
//
//
//
//    //Video player for recurso ID
//    func verVideo( _ req:Request) throws -> Future<View>{
//        let param1 = try req.parameters.next(String.self)
//        let id = UUID.init(param1)
//       return  Recurso.query(on: req).filter(\.id == id).first().flatMap(to: View.self, { (recurso) in
//            guard let recurso = recurso else {
//                throw VaporError(identifier: "001", reason: "Error no encontrado")
//            }
//
//        //  mx/recurso/{id_recurso}/user/{id_user}
//        let recurso_id =  recurso.id!.uuidString
//        let contexto = FavoritoContexto(rid: recurso_id, urlVideo: recurso.urlStorage)
//        return try req.view().render("video", contexto)
//        })
//    }
//
//
//
//
//    func setFavorito(_ req:Request) throws -> Future<String>{
//        let user_id =  Int(try req.session()["id"]!)
//        let usuario =  User.query(on: req).filter(\User.id  == user_id).first().map(to: User.self) { (usuario) in
//            return usuario!
//        }
//        return flatMap(to: String.self,try req.parameters.next(Recurso.self) ,usuario, { (recurso, usuario) in
//            return FavoritoPivot.query(on: req).group(.and) { builder in
//                 builder.filter(\FavoritoPivot.username == usuario.id!)
//                 builder.filter(\FavoritoPivot.recursoID == recurso.id!)
//                }.first().map(to: String.self, { existeFavorito in
//                    guard let existeFavorito = existeFavorito else {
//                        let addFavorito = try FavoritoPivot(usuario.requireID(), recurso.requireID())
//                        let  _ = addFavorito.save(on: req)
//                        return "on"
//                    }
//                     let _ = existeFavorito.delete(on: req)
//                     return "off"
//                })
//        })
//
//    }
//
//
//
//    func getFavorito(_ req:Request) throws -> Future<String>{
//
//        let user_id =  Int(try req.session()["id"]!)
//        let usuario =  User.query(on: req).filter(\User.id  == user_id).first().map(to: User.self) { (usuario) in
//            return usuario!
//        }
//
//        return flatMap(to: String.self, try req.parameters.next(Recurso.self), usuario, { (recurso, usuario)  in
//
//            return FavoritoPivot.query(on: req).group(MySQLBinaryOperator.and, closure: { (pivote) in
//                pivote.filter(\FavoritoPivot.username == usuario.id!)
//                pivote.filter(\FavoritoPivot.recursoID == recurso.id!)
//            }).first().map(to: String.self, { pivoteResultado in
//                guard pivoteResultado != nil else {
//                    return "off"
//                }
//              return "on"
//            })
//
//        })
//
//    }
//
//
//
//
//
//    //AWS S3
//    func getAllResources(_ req:Request) throws -> Future<BucketsInfo>{
//        let s3 = try req.makeS3Client()
//        return try s3.buckets(on: req)
////        return try s3.list(bucket: "mexicocancode", region: .usWest1, headers: [:], on: req).catchMap({ (error) -> (BucketResults) in
////            if let error = error.s3ErroMessage() {
////                print(error.message)
////            }
////            throw error
////        })
//
//    }
//
//
//
//
//    func uploadFile(_ req:Request) throws -> Future<HTTPResponse>{
//        let s3 = try req.makeS3Client()
//        return try req.content.decode(UploadRecurso.self).flatMap({ uploadArchivo in
//            let file = File.Upload(data: uploadArchivo.archivo, bucket: nil, destination: "imagen.jpg", access: AccessControlList.publicRead, mime: "image/jpeg")
//            return try s3.put(file: file, headers: [ : ], on: req).flatMap(to: HTTPResponse.self, { (file)  in
//                return Future.map(on: req, {
//                    return HTTPResponse(status:.ok)
//                })
//            })
//         })
//    }
//
//
//
//
//
//
////SUBIR IMAGEN A AMAZON S3
//
//struct UploadImagenRequest:Content {
//    var upload:Data //or File if want to access file's name
//}
//
//struct ImagenRespuesta:Content{
////Expect response for CKEditor & CKFinder
//        var uploaded:Int
//        var fileName:String
//        var url:String
//        //var error:String
//  }
//
//
//enum FormatoImagen{
//  case jpg
//  case png
//}
//
//func uploadImage(_ req:Request) throws -> Future<ImagenRespuesta>{
//        let s3 = try req.makeS3Client()
//
//    return try req.content.decode(UploadImagenRequest.self).flatMap({ uploadArchivo in
//           //identify Data (image) extension or mimeType
//
//            let file = File.Upload(data: uploadArchivo.upload, bucket: "mexicocancode", destination: "imagen.jpg", access: AccessControlList.publicRead, mime: "image/jpeg")
//
//            //identify Image exits
//            return try self.existeImagenBucket(req, nombreImagen: file.path).flatMap({ existe in
//                if existe == true {
//                    return Future.map(on:req,{
//                        let  respuesta = ImagenRespuesta(uploaded: 0, fileName: "", url: "")
//                       // let  respuesta = ImagenRespuesta(uploaded: 0, fileName: "", url: "", error: "message:\"existe\"")
//                        return respuesta
//
//                    })
//
//                } else {
//
//                return try s3.put(file: file, on: req)
//                    .flatMap(to: ImagenRespuesta.self, { archivoSubido in
//                    return Future.map(on: req, {
//                        let respuesta = ImagenRespuesta(uploaded: 1, fileName:archivoSubido.path, url:"https://s3-us-west-1.amazonaws.com/mexicocancode/" + archivoSubido.path)
//                    print(respuesta)
//                    return respuesta
//                    })
//                 })
//               }
//            })
//        })
//    }
//
//
//    func existeImagenBucket(_ req:Request, nombreImagen:String) throws -> Future<Bool> {
//        let s3 =  try req.makeS3Client()
//
//        return try s3.get(fileInfo: nombreImagen, headers:[:], on: req).flatMap(to: Bool.self, { (existe)  in
//            guard let etag = existe.etag   else{
//                return Future.map( on:req, { return false })
//            }
//             print(etag)
//            return Future.map( on:req, { return true  })
//        }).catchMap({ (error)  in
//            print(error.localizedDescription)
//            return  false
//        })
//
//    }
//
//
//
//
//
//
//
//
////SUBIR IMAGEN PERFIL
//
//    func uploadPerfilTest(_ req: Request) throws -> Future<HTTPResponse>{
//        return try req.content.decode(UploadPerfil.self).map(to: HTTPResponse.self, { perfil  in
//            print(perfil)
//            return HTTPResponse(status:.ok)
//        })
//    }
//
//
//
//    func uploadFileGet(_ req: Request) throws -> Future<View>{
//        return try req.view().render("upload")
//    }
//
//
//
//
//    //Relationship
//
//    func addCategorias(_ req:Request) throws -> Future<HTTPStatus>{
//        return flatMap(to: HTTPStatus.self, try req.parameters.next(Recurso.self), try req.parameters.next(Categoria.self), { (recurso, categoria) in
//
//            let pivot = try RecursoCategoriaPivot(recursoID: recurso.requireID(), categoriaID: categoria.requireID())
//
//            return pivot.save(on: req).transform(to: .created)
//        })
//    }
//
//
//
//    func getAllCategorias(_ req:Request) throws -> Future<[Categoria]>{
//        return try req.parameters.next(Recurso.self).flatMap(to: [Categoria].self, { (recurso) in
//            return try recurso.categorias.query(on: req).all()
//        })
//    }
//
//
//
//    func getAllCategoriasView(_ req: Request) throws -> Future<View>{
//            let categorias = Categoria.query(on:req).all()
//        let contexto =  AllCategoriaContexto(categorias: categorias)
//         return try req.view().render( "recursos", contexto)
//    }
//
//
//
//    func getAllRecursosDeCategoriaView(_ req: Request) throws -> Future<View>{
//        let recursos = try req.parameters.next(Categoria.self).flatMap(to: [Recurso].self, { categoria in
//            return try categoria.recursos.query(on: req).all()
//        })
//
//        let sesion = try req.session()
//        if sesion.id != nil {
//         let contexto = AllRecursoContexto(name: sesion["name"]!, role: sesion["role"]!, recursos: recursos)
//            return try req.view().render("categoria", contexto)
//        }
//        return try req.view().render("login")
//   }
//
   
    
    
    
    
}

