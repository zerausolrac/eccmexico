import Vapor
import FluentMySQL

final class RegistroController:RouteCollection{
    func boot(router: Router) throws {
        
        //ADEs Summit route
        router.get("ademx2018", use:adesummit)
        router.post(RegistroContexto.self, at: "registro", use: register)
        
    }
    
    
    
    func adesummit(_ req:Request) throws -> Future<View>{
        return try req.view().render("adesummit")
    }
    
    
    
    
    func register(_ req: Request, datos:RegistroContexto) throws -> Future<View>{
        
        let contexto = RegistroContexto(nombre: datos.nombre, email: datos.email, clase: datos.clase, area: datos.area, embajador: Int(datos.embajador), autor: Int(datos.autor), consejero: Int(datos.consejero), abogado: Int(datos.abogado), razon: datos.razon, equipo: Int(datos.equipo), apps: datos.apps, razon2: datos.razon2, alergia: Int(datos.alergia), playera: datos.playera, agree: Int(datos.agree))
        
        print("apps: \(String(describing: datos.apps))")
        print(contexto)
        
        return try req.view().render("confirmacion", contexto)
    }
    
    
}


struct RegistroContexto:Content{
    var nombre:String
    var email:String
    var clase:String
    var area:String
    var embajador:Int
    var autor:Int
    var consejero:Int
    var abogado:Int
    var razon:String
    var equipo:Int
    var apps:[String]?
    var razon2:String
    var alergia:Int
    var playera: String
    var agree:Int
}

