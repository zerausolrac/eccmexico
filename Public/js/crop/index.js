// vars
var result = document.querySelector('.result'),
    img_result = document.querySelector('.img-result'),
    img_w = document.querySelector('.img-w'),
    img_h = document.querySelector('.img-h'),
    show = document.querySelector('.save'),
    cropped = document.querySelector('.cropped'),
    upload = document.querySelector('#file-input'),
    formbtn = document.querySelector('#formbtn'),
    cropper = '';

var perfilform = document.getElementById('#perfilform');


// on change show image with crop options
upload.addEventListener('change', function (e) {
  if (e.target.files.length) {
    // start file reader
    var reader = new FileReader();
    reader.onload = function (e) {
      if (e.target.result) {
        // create new image
        var img = document.createElement('img');
        img.id = 'image';
        img.src = e.target.result;
        // clean result before
        result.innerHTML = '';
        // append new image
        result.appendChild(img);
        // show save btn and options
        show.classList.remove('hide');
                        
        //cropper = new Cropper(img);
                        
        cropper = new Cropper(img, {
                                    dragMode: 'move',
                                    aspectRatio: 1,
                                    autoCropArea: 0.65,
                                    restore: false,
                                    guides: true,
                                    center: false,
                                    highlight: false,
                                    cropBoxMovable: true,
                                    cropBoxResizable: false,
                                    toggleDragModeOnDblclick: false,
                                    minCropBoxWidth: 300,
                                    minCropBoxHeight: 300,
                                    });
      }
    };
    reader.readAsDataURL(e.target.files[0]);
  }
});





// save on click
  show.addEventListener('click', function (e) {
  e.preventDefault();
  // get result to data uri
  var imgSrc = cropper.getCroppedCanvas({ width: 300 }).toDataURL();
  // remove hide class of img
  cropped.classList.remove('hide');
  img_result.classList.remove('hide');
  // show image cropped
  cropped.src = imgSrc;
});


formbtn.addEventListener('click', function(e) {
    
      cropper.getCroppedCanvas().toBlob(function (blob) {
      var formData = new FormData();
      formData.append('avatar', blob);
      
      //Filename
      var filename = $("#file-input").get(0).files[0];
      
                            
       $('#pefilupdate').val(filename.name);
       //var elemento = $('#pefilupdate').serializeArray();

                                        
       formData.append("filename", filename.name);
                                        
      
                                        
                                        
                                        
                
       $.ajax('/perfilupdate', {
            method: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function () {
            console.log('Upload success' + avatar);
           },
              error: function () {
                console.log('Upload error');
            }
          });
        });
                            
});


